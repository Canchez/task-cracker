package com.example.taskcracker.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.example.taskcracker.R
import com.example.taskcracker.databinding.EditTaskBinding
import com.example.taskcracker.fragments.TasksFragment
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class EditTaskActivity : Activity() {
    private lateinit var editTaskBinding: EditTaskBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        editTaskBinding = EditTaskBinding.inflate(layoutInflater)
        setContentView(editTaskBinding.root)

        setupPickers()

        editTaskBinding.editTaskDate.setOnClickListener {
            val intent = Intent(this, CalendarActivity::class.java)
            startActivityForResult(intent, TasksFragment.CALENDAR_ACTIVITY_REQUEST_CODE)
        }

        val returnIntent = Intent()

        val extras = intent.extras
        if (extras != null) {
            val taskId = extras.getInt(EDIT_TASK_ID, -1)
            val taskName = extras.getString(EDIT_TASK_NAME)
            val taskDate = extras.getString(EDIT_TASK_DATE)
            val taskStartTime = extras.getString(EDIT_TASK_START)
            val elapsedTime = extras.getLong(EDIT_TASK_ELAPSED, -1)
            if (
                taskId == -1 ||
                taskName == null ||
                taskDate == null ||
                taskStartTime == null ||
                elapsedTime == (-1).toLong()
            ) {
                setResult(RESULT_ERROR)
                finish()
            } else {

                setupUiWithValues(taskName, taskDate, taskStartTime, elapsedTime)

                editTaskBinding.cancelEditButton.setOnClickListener {
                    setResult(RESULT_CANCELED)
                    finish()
                }

                editTaskBinding.confirmEditButton.setOnClickListener {
                    if (editTaskBinding.editTaskTitle.text.isNotEmpty() &&
                        editTaskBinding.editTaskDate.text.isNotEmpty()
                    ) {
                        val startTimeStr = getStartStrFromPickers()
                        val editedElapsed = getMilSecFromPickers()
                        returnIntent
                            .putExtra(EDIT_TASK_ID, taskId)
                            .putExtra(
                                EDIT_TASK_NAME,
                                editTaskBinding.editTaskTitle.text.toString()
                            )
                            .putExtra(EDIT_TASK_DATE, editTaskBinding.editTaskDate.text)
                            .putExtra(EDIT_TASK_START, startTimeStr)
                            .putExtra(EDIT_TASK_ELAPSED, editedElapsed)
                        setResult(RESULT_EDIT, returnIntent)
                        finish()
                    } else {
                        Toast.makeText(
                            this,
                            "Don't leave empty fields!",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }

                editTaskBinding.deleteButton.setOnClickListener {
                    val deleteDialogBuilder = AlertDialog.Builder(this)
                    deleteDialogBuilder
                        .setMessage(getString(R.string.deleteConfirmStr))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.confirmDeleteStr)) { _, _ ->
                            returnIntent.putExtra(EDIT_TASK_ID, taskId)
                            setResult(RESULT_DELETE, returnIntent)
                            finish()
                        }
                        .setNegativeButton(getString(R.string.cancelStr)) { dialog, _ ->
                            dialog.dismiss()
                        }
                    val deleteDialog = deleteDialogBuilder.create()
                    deleteDialog.show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == TasksFragment.CALENDAR_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                val dateStr = data.getStringExtra(TasksFragment.CHOSEN_DATE_STR)
                editTaskBinding.editTaskDate.text = dateStr
            }
        }
    }

    private fun setupUiWithValues(
        taskName: String,
        taskDate: String,
        taskStartTime: String,
        elapsedTime: Long,
    ) {
        editTaskBinding.editTaskTitle.setText(taskName)
        editTaskBinding.editTaskDate.text = taskDate

        val startTime = LocalTime.parse(
            taskStartTime,
            DateTimeFormatter.ofPattern(getString(R.string.timeFormatStr))
        )
        editTaskBinding.editTaskStartedHour.value = startTime.hour
        editTaskBinding.editTaskStartedMinute.value = startTime.minute
        editTaskBinding.editTaskStartedSecond.value = startTime.second

        val elapsedSec = elapsedTime / 1000
        val elapsedSecPart = elapsedSec % 60
        val elapsedMinPart = elapsedSec / 60 % 60
        val elapsedHourPart = elapsedSec / 60 / 60
        editTaskBinding.editElapsedHour.value = elapsedHourPart.toInt()
        editTaskBinding.editElapsedMinute.value = elapsedMinPart.toInt()
        editTaskBinding.editElapsedSecond.value = elapsedSecPart.toInt()
    }

    private fun setupPickers() {
        editTaskBinding.editTaskStartedHour.minValue = MIN_TIME
        editTaskBinding.editTaskStartedHour.maxValue = MAX_HOUR
        editTaskBinding.editTaskStartedMinute.minValue = MIN_TIME
        editTaskBinding.editTaskStartedMinute.maxValue = MAX_MIN_AND_SEC
        editTaskBinding.editTaskStartedSecond.minValue = MIN_TIME
        editTaskBinding.editTaskStartedSecond.maxValue = MAX_MIN_AND_SEC

        editTaskBinding.editElapsedHour.minValue = MIN_TIME
        editTaskBinding.editElapsedHour.maxValue = MAX_TIME
        editTaskBinding.editElapsedMinute.minValue = MIN_TIME
        editTaskBinding.editElapsedMinute.maxValue = MAX_MIN_AND_SEC
        editTaskBinding.editElapsedSecond.minValue = MIN_TIME
        editTaskBinding.editElapsedSecond.maxValue = MAX_MIN_AND_SEC
    }

    private fun getMilSecFromPickers(): Long {
        return ((editTaskBinding.editElapsedHour.value * 60 * 60 +
                editTaskBinding.editElapsedMinute.value * 60 +
                editTaskBinding.editTaskStartedSecond.value) * 1000).toLong()
    }

    private fun getStartStrFromPickers(): String {
        val startTime = LocalTime.of(
            editTaskBinding.editTaskStartedHour.value,
            editTaskBinding.editTaskStartedMinute.value,
            editTaskBinding.editTaskStartedSecond.value
        )
        return startTime.format(
            DateTimeFormatter
                .ofPattern(getString(R.string.timeFormatStr))
        )
    }

    companion object {
        const val EDIT_TASK_ID = "taskId"
        const val EDIT_TASK_NAME = "taskName"
        const val EDIT_TASK_DATE = "taskDate"
        const val EDIT_TASK_START = "taskStartTime"
        const val EDIT_TASK_ELAPSED = "taskElapsed"

        const val EDIT_TASK_ACTIVITY_REQUEST_CODE = 2

        const val RESULT_ERROR = 2
        const val RESULT_DELETE = 3
        const val RESULT_EDIT = 4

        const val MAX_MIN_AND_SEC = 59
        const val MAX_HOUR = 23
        const val MIN_TIME = 0
        const val MAX_TIME = 999
    }
}