package com.example.taskcracker.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.taskcracker.R
import com.example.taskcracker.TaskFragmentsAdapter
import com.example.taskcracker.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = TaskFragmentsAdapter(this)
        val viewPager = binding.viewpager
        viewPager.adapter = adapter
        val tabLayout = binding.tabLayout
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.timerTabText)
                else -> getString(R.string.tasksTabText)
            }
        }.attach()
    }

}