package com.example.taskcracker.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.CalendarView
import com.example.taskcracker.R
import com.example.taskcracker.databinding.CalendarBinding
import com.example.taskcracker.fragments.TasksFragment
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CalendarActivity : Activity() {
    private lateinit var calendarBinding: CalendarBinding
    private lateinit var chosenDate: LocalDate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        calendarBinding = CalendarBinding.inflate(layoutInflater)
        setContentView(calendarBinding.root)

        chosenDate = LocalDate.now()
        calendarBinding.confirmDateButton.setOnClickListener {
            val returnIntent = Intent()
            val dateStr = chosenDate.format(
                DateTimeFormatter
                    .ofPattern(getString(R.string.dateFormatStr))
            )
            returnIntent.putExtra(TasksFragment.CHOSEN_DATE_STR, dateStr)
            setResult(RESULT_OK, returnIntent)
            finish()
        }

        calendarBinding.cancelCalendarButton.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }

        calendarBinding.calendarView.setOnDateChangeListener { _: CalendarView,
                                                               year: Int,
                                                               monthOfYear: Int,
                                                               dayOfMonth: Int ->
            chosenDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
        }
    }
}