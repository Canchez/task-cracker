package com.example.taskcracker.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val DB_NAME = "task.db"

@Database(entities = [TaskEntity::class], version = 1)
abstract class TaskDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao

    companion object {
        @Volatile
        private var INSTANCE: TaskDatabase? = null

        @Synchronized
        operator fun get(context: Context): TaskDatabase {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(context, TaskDatabase::class.java, DB_NAME)
                        .build()
            }
            return INSTANCE!!
        }
    }
}