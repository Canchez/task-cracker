package com.example.taskcracker.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface TaskDao {
    @Query("SELECT * FROM tasks WHERE task_date = :date")
    suspend fun findByDate(date: String): List<TaskEntity>

    @Insert
    suspend fun insertTask(task: TaskEntity)

    @Query("DELETE FROM tasks WHERE id = :id")
    suspend fun deleteById(id: Int)

    @Update
    suspend fun updateTask(task: TaskEntity)
}