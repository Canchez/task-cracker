package com.example.taskcracker.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class TaskEntity(
    @ColumnInfo(name = "task_name")
    var taskName: String = "",
    @ColumnInfo(name = "task_date")
    var taskDate: String = "",
    @ColumnInfo(name = "task_start_time")
    var taskStartTime: String = "",
    @ColumnInfo(name = "elapsed_time")
    var elapsedTime: Long = 0,
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}