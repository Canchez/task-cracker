package com.example.taskcracker

interface OnItemListener {
    fun onItemClick(position: Int)
}