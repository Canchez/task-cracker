package com.example.taskcracker.model

import androidx.lifecycle.MutableLiveData
import com.example.taskcracker.room.TaskEntity

class TaskLiveData : MutableLiveData<List<TaskEntity>>()