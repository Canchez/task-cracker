package com.example.taskcracker.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.taskcracker.room.TaskDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TaskViewModel(application: Application) : AndroidViewModel(application) {
    var tasksLiveData = TaskLiveData()

    fun getTasksFromDB(date: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val db = TaskDatabase[getApplication()]

            tasksLiveData.postValue(db.taskDao().findByDate(date))
        }
    }
}