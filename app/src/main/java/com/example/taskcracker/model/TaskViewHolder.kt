package com.example.taskcracker.model

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.taskcracker.OnItemListener
import com.example.taskcracker.R
import com.example.taskcracker.databinding.RowBinding
import com.example.taskcracker.room.TaskEntity

class TaskViewHolder(
    private val binding: RowBinding,
    private val onItemListener: OnItemListener,
) :
    RecyclerView.ViewHolder(binding.root), View.OnClickListener {
    fun bindTo(position: Int, task: TaskEntity) {
        binding.numberTextView.text = (position + 1).toString()
        binding.taskNameTextView.text = task.taskName
        binding.startTimeTextView.text = task.taskStartTime
        binding.elapsedTimeTextView.text = formatElapsedTime(task.elapsedTime)

        binding.root.setOnClickListener(this)
    }

    private fun formatElapsedTime(elapsed: Long): String {
        var formattedSir: String
        val elapsedSec = elapsed / 1000
        val hoursPart = elapsedSec / (60 * 60)
        val minutesPart = elapsedSec / 60 % 60
        val secondsPart = elapsedSec % 60
        formattedSir = "${secondsPart}${binding.root.context.getString(R.string.secondSuf)}"
        if (minutesPart > 0) {
            formattedSir = "${minutesPart}${binding.root.context.getString(R.string.minuteSuf)} " +
                    formattedSir
        }
        if (hoursPart > 0) {
            formattedSir = "${hoursPart}${binding.root.context.getString(R.string.hourSuf)} " +
                    formattedSir
        }
        return formattedSir
    }

    override fun onClick(v: View?) {
        onItemListener.onItemClick(adapterPosition)
    }
}