package com.example.taskcracker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.taskcracker.databinding.RowBinding
import com.example.taskcracker.model.TaskViewHolder
import com.example.taskcracker.room.TaskEntity

class TaskRecyclerAdapter(
    private val inflater: LayoutInflater,
    private val onItemListener: OnItemListener,
) : ListAdapter<TaskEntity, TaskViewHolder>(TaskDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(
            RowBinding.inflate(inflater, parent, false),
            onItemListener,
        )
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bindTo(position, getItem(position))
    }


    private object TaskDiffer : DiffUtil.ItemCallback<TaskEntity>() {
        override fun areItemsTheSame(oldItem: TaskEntity, newItem: TaskEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TaskEntity, newItem: TaskEntity): Boolean {
            return oldItem.taskName == newItem.taskName
                    && oldItem.taskDate == newItem.taskDate
                    && oldItem.taskStartTime == newItem.taskStartTime
                    && oldItem.elapsedTime == newItem.elapsedTime
        }
    }
}