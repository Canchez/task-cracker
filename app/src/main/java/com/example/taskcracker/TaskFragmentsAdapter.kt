package com.example.taskcracker

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.taskcracker.fragments.TasksFragment
import com.example.taskcracker.fragments.TimerFragment

class TaskFragmentsAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
    override fun getItemCount(): Int {
        return TABS_AMOUNT
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> TimerFragment()
            else -> TasksFragment()
        }
    }

    companion object {
        private const val TABS_AMOUNT = 2
    }
}