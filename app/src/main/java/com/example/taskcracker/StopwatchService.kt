package com.example.taskcracker

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.example.taskcracker.activities.MainActivity
import com.example.taskcracker.fragments.TimerFragment
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class StopwatchService : Service() {
    var serviceHandler = Handler(Looper.getMainLooper())
    lateinit var clockCurrentStartPoint: LocalDateTime
    var savedTime: Long? = 0L
    var elapsedTime = 0L
    private var taskName: String? = ""
    lateinit var notificationBuilder: NotificationCompat.Builder
    lateinit var notificationManager: NotificationManager

    private var runnableClock: Runnable = object : Runnable {
        override fun run() {
            val timeBetween = Duration.between(clockCurrentStartPoint, LocalDateTime.now())
            val millisecondsBetween = timeBetween.toMillis() + savedTime!!
            elapsedTime = millisecondsBetween
            notificationBuilder.setContentText(getElapsedTimeString(elapsedTime))
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
            serviceHandler.postDelayed(this, 1000)
        }
    }

    private fun getElapsedTimeString(mills: Long): String {
        val secondsPassed = (mills / 1000).toInt()
        val secondsToShow = secondsPassed % 60
        val minutesToShow = secondsPassed / 60 % 60
        val hoursToShow = secondsPassed / (60 * 60)

        val secondsStr = TimerFragment.formatNumberToStr(secondsToShow)
        val minutesStr = TimerFragment.formatNumberToStr(minutesToShow)
        val hoursStr = TimerFragment.formatNumberToStr(hoursToShow)

        return "${hoursStr}:${minutesStr}:$secondsStr"
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val currentPointStr = intent?.getStringExtra(TimerFragment.CLOCK_CURRENT_POINT)
        clockCurrentStartPoint = currentPointStr?.let {
            LocalDateTime
                .parse(it, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        } ?: LocalDateTime.now()
        clockCurrentStartPoint = LocalDateTime
            .parse(currentPointStr, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        savedTime = intent?.getLongExtra(TimerFragment.SAVED_TIME, 0)
        taskName = intent?.getStringExtra(TimerFragment.TASK_NAME)

        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivities(
            this,
            0,
            arrayOf(notificationIntent),
            0
        )

        if (savedTime != null && !taskName.isNullOrEmpty()) {
            notificationManager = getSystemService(NotificationManager::class.java)
            notificationBuilder = NotificationCompat.Builder(this, App.CHANNEL_ID)
                .setContentTitle(taskName)
                .setContentText(getElapsedTimeString(savedTime!!))
                .setSmallIcon(R.drawable.ic_baseline_timer)
                .setSilent(true)
                .setContentIntent(pendingIntent)
                .setOnlyAlertOnce(true)
            startForeground(NOTIFICATION_ID, notificationBuilder.build())
            serviceHandler.post(runnableClock)
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        serviceHandler.removeCallbacks(runnableClock)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    companion object {
        const val NOTIFICATION_ID = 1
    }
}