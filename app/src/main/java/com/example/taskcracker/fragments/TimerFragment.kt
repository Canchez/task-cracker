package com.example.taskcracker.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.taskcracker.R
import com.example.taskcracker.StopwatchService
import com.example.taskcracker.databinding.TimerFragmentBinding
import com.example.taskcracker.room.TaskDatabase
import com.example.taskcracker.room.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class TimerFragment : Fragment() {
    private var isClockStarted = false
    private var isClockPaused = false
    private var clockStartMoment: LocalDateTime = LocalDateTime.now()
    var clockCurrentStartPoint: LocalDateTime = LocalDateTime.now()
    var savedTime = 0L
    var elapsedTime = 0L
    var clockHandler = Handler(Looper.getMainLooper())

    private lateinit var timerBinding: TimerFragmentBinding


    private var runnableClock: Runnable = object : Runnable {
        override fun run() {
            val timeBetween = Duration.between(clockCurrentStartPoint, LocalDateTime.now())
            val millisecondsBetween = timeBetween.toMillis() + savedTime
            elapsedTime = millisecondsBetween
            updateViews(millisecondsBetween)
            clockHandler.postDelayed(this, 200)
        }
    }


    fun updateViews(mills: Long) {
        val secondsPassed = (mills / 1000).toInt()
        val secondsToShow = secondsPassed % 60
        val minutesToShow = secondsPassed / 60 % 60
        val hoursToShow = secondsPassed / (60 * 60)

        val secondsStr = formatNumberToStr(secondsToShow)
        val minutesStr = formatNumberToStr(minutesToShow)
        val hoursStr = formatNumberToStr(hoursToShow)

        timerBinding.secondsText.text = secondsStr
        timerBinding.minutesText.text = minutesStr
        timerBinding.hoursText.text = hoursStr
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        timerBinding = TimerFragmentBinding.inflate(inflater, container, false)
        return timerBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        timerBinding.timerControlButton.setOnClickListener {
            handleControlAction()
        }
        timerBinding.editTaskName.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                handleControlAction()
                true
            } else {
                false
            }
        }
        timerBinding.timerResetButton.setOnClickListener {
            val alertDialogBuilder = AlertDialog.Builder(context)
            alertDialogBuilder
                .setMessage(getString(R.string.resetConfirmStr))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.confirmResetStr)) { _, _ ->
                    isClockStarted = false
                    isClockPaused = false
                    resetClock()
                    changeUIState(INITIAL_STATE_STR)
                }
                .setNegativeButton(getString(R.string.cancelStr)) { dialog, _ ->
                    dialog.dismiss()
                }
            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()
        }
        timerBinding.timerSaveButton.setOnClickListener {
            val taskEntity = TaskEntity()

            taskEntity.taskName = timerBinding.editTaskName.text.toString()
            taskEntity.taskDate = clockStartMoment.toLocalDate()
                .format(DateTimeFormatter.ofPattern(getString(R.string.dateFormatStr)))
            taskEntity.taskStartTime = clockStartMoment.toLocalTime()
                .format(DateTimeFormatter.ofPattern(getString(R.string.timeFormatStr)))
            taskEntity.elapsedTime = elapsedTime

            MainScope().launch {
                try {
                    saveTaskToDB(taskEntity)
                } catch (t: Throwable) {
                    Toast.makeText(
                        context,
                        getString(R.string.saveFailedStr),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            isClockStarted = false
            isClockPaused = false
            resetClock()
            changeUIState(INITIAL_STATE_STR)
        }
    }

    private suspend fun saveTaskToDB(task: TaskEntity) = withContext(Dispatchers.IO) {
        val db = TaskDatabase[requireContext()]

        db.taskDao().insertTask(task)
    }

    private fun checkStateAndChangeState() {
        if (!isClockStarted) {
            isClockStarted = true
            isClockPaused = false
            startClock()
            changeUIState(RUNNING_STATE_STR)
        } else if (isClockStarted && !isClockPaused) {
            isClockPaused = true
            pauseClock()
            changeUIState(PAUSED_STATE_STR)
        } else if (isClockStarted && isClockPaused) {
            isClockPaused = false
            resumeClock()
            changeUIState(RUNNING_STATE_STR)
        }
    }

    private fun startClock() {
        clockStartMoment = LocalDateTime.now()
        clockCurrentStartPoint = clockStartMoment
        savedTime = 0
        elapsedTime = 0
        clockHandler.post(runnableClock)
    }

    private fun pauseClock() {
        clockHandler.removeCallbacks(runnableClock)
        savedTime = elapsedTime
    }

    private fun resumeClock() {
        clockCurrentStartPoint = LocalDateTime.now()
        clockHandler.post(runnableClock)

    }

    private fun resetClock() {
        savedTime = 0
        elapsedTime = 0
        updateViews(0)
    }

    private fun changeUIState(state: String) {
        when (state) {
            INITIAL_STATE_STR -> {
                timerBinding.timerControlButton.foreground = context
                        ?.getDrawable(R.drawable.ic_play_arrow)
                changeButtonsOpacity(INACTIVE_ALPHA)
                timerBinding.timerSaveButton.isEnabled = false
                timerBinding.timerResetButton.isEnabled = false
                timerBinding.editTaskName.setText("")
                timerBinding.editTaskName.isEnabled = true
            }
            RUNNING_STATE_STR -> {
                timerBinding.timerControlButton.foreground = context
                    ?.getDrawable(R.drawable.ic_pause)
                changeButtonsOpacity(INACTIVE_ALPHA)
                timerBinding.timerSaveButton.isEnabled = false
                timerBinding.timerResetButton.isEnabled = false
                timerBinding.editTaskName.isEnabled = false
            }
            PAUSED_STATE_STR -> {
                timerBinding.timerControlButton.foreground = context
                    ?.getDrawable(R.drawable.ic_play_arrow)
                changeButtonsOpacity(ACTIVE_ALPHA)
                timerBinding.timerSaveButton.isEnabled = true
                timerBinding.timerResetButton.isEnabled = true
                timerBinding.editTaskName.isEnabled = false
            }
        }
    }

    private fun changeButtonsOpacity(opacity: Float) {
        timerBinding.timerResetButton.alpha = opacity
        timerBinding.timerSaveButton.alpha = opacity
    }

    override fun onStart() {
        super.onStart()
        val prefs = context?.getSharedPreferences(SAVED_PREFS, Context.MODE_PRIVATE)
        val tmpCurrentPoint: String
        val tmpStartMoment: String
        var tmpTaskName = ""
        if (prefs != null) {
            tmpCurrentPoint = prefs.getString(CLOCK_CURRENT_POINT, LocalDateTime.now().toString())
                .toString()
            clockCurrentStartPoint = LocalDateTime.parse(
                tmpCurrentPoint,
                DateTimeFormatter.ISO_LOCAL_DATE_TIME
            )
            tmpStartMoment = prefs.getString(CLOCK_START_MOMENT, LocalDateTime.now().toString())
                .toString()
            clockStartMoment = LocalDateTime.parse(
                tmpStartMoment,
                DateTimeFormatter.ISO_LOCAL_DATE_TIME
            )
            tmpTaskName = prefs.getString(TASK_NAME, "").toString()
            savedTime = prefs.getLong(SAVED_TIME, 0)
            isClockStarted = prefs.getBoolean(IS_STARTED, false)
            isClockPaused = prefs.getBoolean(IS_PAUSED, false)
        }
        timerBinding.editTaskName.setText(tmpTaskName)
        if (isClockStarted && !isClockPaused) {
            clockHandler.post(runnableClock)
            changeUIState(RUNNING_STATE_STR)
        }
        if (isClockStarted && isClockPaused) {
            changeUIState(PAUSED_STATE_STR)
            updateViews(savedTime)
        }

        stopStopwatchService()
    }

    override fun onStop() {
        super.onStop()
        val currentPointStr = clockCurrentStartPoint.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        val startMomentStr = clockStartMoment.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        val taskName = timerBinding.editTaskName.text.toString()
        val prefs = context?.getSharedPreferences(SAVED_PREFS, Context.MODE_PRIVATE)
        if (prefs != null) {
            with(prefs.edit()) {
                putString(CLOCK_START_MOMENT, startMomentStr)
                putString(CLOCK_CURRENT_POINT, currentPointStr)
                putString(TASK_NAME, taskName)
                putLong(SAVED_TIME, savedTime)
                putBoolean(IS_STARTED, isClockStarted)
                putBoolean(IS_PAUSED, isClockPaused)
                apply()
            }
        }
        if (isClockStarted && !isClockPaused) {
            startStopwatchService(
                currentPointStr,
                savedTime,
                taskName
            )
        }
    }

    private fun startStopwatchService(
        currentStartPoint: String,
        savedTime: Long,
        taskName: String,
    ) {
        val serviceIntent = Intent(context, StopwatchService::class.java)
        serviceIntent
            .putExtra(CLOCK_CURRENT_POINT, currentStartPoint)
            .putExtra(SAVED_TIME, savedTime)
            .putExtra(TASK_NAME, taskName)
        context?.startService(serviceIntent)
    }

    private fun stopStopwatchService() {
        val serviceIntent = Intent(context, StopwatchService::class.java)
        context?.stopService(serviceIntent)
    }

    private fun handleControlAction() {
        if (timerBinding.editTaskName.text.toString().isEmpty()) {
            Toast.makeText(
                context,
                getString(R.string.taskNotEmptyStr),
                Toast.LENGTH_LONG
            ).show()
        } else {
            checkStateAndChangeState()
        }
    }

    companion object {
        const val INITIAL_STATE_STR = "Initial"
        const val RUNNING_STATE_STR = "Running"
        const val PAUSED_STATE_STR = "Paused"
        const val ACTIVE_ALPHA = 1.0F
        const val INACTIVE_ALPHA = 0.5F

        // SharedPreferences Strings
        const val SAVED_PREFS = "savedPrefs"
        const val CLOCK_CURRENT_POINT = "clockCurrentStartPoint"
        const val CLOCK_START_MOMENT = "clockStartMoment"
        const val TASK_NAME = "taskName"
        const val SAVED_TIME = "savedTime"
        const val IS_STARTED = "isStarted"
        const val IS_PAUSED = "isPaused"

        fun formatNumberToStr(number: Int): String {
            var str = number.toString()
            if (number < 10) {
                str = "0${str}"
            }
            return str
        }
    }
}