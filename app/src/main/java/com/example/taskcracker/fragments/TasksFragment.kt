package com.example.taskcracker.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.taskcracker.OnItemListener
import com.example.taskcracker.R
import com.example.taskcracker.TaskRecyclerAdapter
import com.example.taskcracker.activities.CalendarActivity
import com.example.taskcracker.activities.EditTaskActivity
import com.example.taskcracker.databinding.TasksFragmentBinding
import com.example.taskcracker.model.TaskViewModel
import com.example.taskcracker.room.TaskDatabase
import com.example.taskcracker.room.TaskEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class TasksFragment : Fragment(), OnItemListener {
    private val taskVM: TaskViewModel by viewModels()
    private lateinit var currentDate: LocalDate
    private lateinit var tasksBinding: TasksFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        tasksBinding = TasksFragmentBinding.inflate(inflater, container, false)
        return tasksBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val taskAdapter = TaskRecyclerAdapter(layoutInflater, this)

        tasksBinding.taskRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            adapter = taskAdapter
        }

        taskVM.tasksLiveData.observe(viewLifecycleOwner) {
            taskAdapter.submitList(it)
            taskAdapter.notifyDataSetChanged()
        }

        currentDate = LocalDate.now()
        tasksBinding.nextDateButton.setOnClickListener {
            currentDate = currentDate.plusDays(1)
            updateUI(currentDate)
        }
        tasksBinding.prevDateButton.setOnClickListener {
            currentDate = currentDate.minusDays(1)
            updateUI(currentDate)
        }

        tasksBinding.chosenDateTextView.setOnClickListener {
            val intent = Intent(context, CalendarActivity::class.java)
            startActivityForResult(intent, CALENDAR_ACTIVITY_REQUEST_CODE)
        }
    }

    private fun updateUI(date: LocalDate) {
        val dateStr = date
            .format(DateTimeFormatter.ofPattern(getString(R.string.dateFormatStr)))
        tasksBinding.chosenDateTextView.text = dateStr
        taskVM.getTasksFromDB(dateStr)
        tasksBinding.taskRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        updateUI(currentDate)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CALENDAR_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == RESULT_OK && data != null) {
                    val dateStr = data.getStringExtra(CHOSEN_DATE_STR)
                    val date = LocalDate.parse(
                        dateStr,
                        DateTimeFormatter.ofPattern(getString(R.string.dateFormatStr))
                    )
                    currentDate = date
                    updateUI(currentDate)
                }
            }
            EditTaskActivity.EDIT_TASK_ACTIVITY_REQUEST_CODE -> {
                when (resultCode) {
                    EditTaskActivity.RESULT_ERROR -> {
                        Toast.makeText(
                            requireContext(),
                            "Error occurred!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                    EditTaskActivity.RESULT_EDIT -> {
                        if (data != null) {
                            val editedTaskId = data
                                .getIntExtra(
                                    EditTaskActivity.EDIT_TASK_ID,
                                    -1
                                )
                            val editedTaskName = data
                                .getStringExtra(EditTaskActivity.EDIT_TASK_NAME)
                            val editedTaskDate = data
                                .getStringExtra(EditTaskActivity.EDIT_TASK_DATE)
                            val editedTaskStart = data
                                .getStringExtra(EditTaskActivity.EDIT_TASK_START)
                            val editedTaskElapsed = data
                                .getLongExtra(
                                    EditTaskActivity.EDIT_TASK_ELAPSED,
                                    -1
                                )
                            if (
                                editedTaskId != -1 &&
                                !editedTaskName.isNullOrEmpty() &&
                                !editedTaskDate.isNullOrEmpty() &&
                                !editedTaskStart.isNullOrEmpty() &&
                                editedTaskElapsed != (-1).toLong()
                            ) {
                                val taskToUpdate = TaskEntity(
                                    editedTaskName,
                                    editedTaskDate,
                                    editedTaskStart,
                                    editedTaskElapsed
                                )
                                taskToUpdate.id = editedTaskId
                                MainScope().launch(Dispatchers.IO) {
                                    updateTask(taskToUpdate)
                                }
                                updateUI(currentDate)
                            }
                        }
                    }
                    EditTaskActivity.RESULT_DELETE -> {
                        if (data != null) {
                            val taskToDeleteId = data
                                .getIntExtra(
                                    EditTaskActivity.EDIT_TASK_ID,
                                    -1
                                )
                            if (taskToDeleteId != -1) {
                                MainScope().launch(Dispatchers.IO) {
                                    deleteTaskFromDb(taskToDeleteId)
                                }
                                updateUI(currentDate)
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun deleteTaskFromDb(id: Int) {
        val db = TaskDatabase[requireContext()]
        db.taskDao().deleteById(id)
    }

    private suspend fun updateTask(task: TaskEntity) {
        val db = TaskDatabase[requireContext()]
        db.taskDao().updateTask(task)
    }

    override fun onItemClick(position: Int) {
        val clickedTask = taskVM.tasksLiveData.value?.get(position)
        if (clickedTask != null) {
            val editIntent = Intent(context, EditTaskActivity::class.java)
            editIntent
                .putExtra(EditTaskActivity.EDIT_TASK_ID, clickedTask.id)
                .putExtra(EditTaskActivity.EDIT_TASK_NAME, clickedTask.taskName)
                .putExtra(EditTaskActivity.EDIT_TASK_DATE, clickedTask.taskDate)
                .putExtra(EditTaskActivity.EDIT_TASK_START, clickedTask.taskStartTime)
                .putExtra(EditTaskActivity.EDIT_TASK_ELAPSED, clickedTask.elapsedTime)
            startActivityForResult(editIntent, EditTaskActivity.EDIT_TASK_ACTIVITY_REQUEST_CODE)
        }
    }


    companion object {
        const val CALENDAR_ACTIVITY_REQUEST_CODE = 1
        const val CHOSEN_DATE_STR = "chosenDate"
    }
}